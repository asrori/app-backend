<?php

namespace App\Http\Controllers;
use App\Mahasiswa;
use Illuminate\Http\Request;
use Validator;

class MahasiswaController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function index(Request $request)
    {
        //
        $search  = $request->get('search');
        $sortBy  = $request->get('sortBy', 'created_at');
        $orderBy = $request->get('descending', 'false') == 'true' ? 'DESC' : 'ASC';
        $perPage = (int) $request->get('per_page', 15);

        $mahasiswa = Mahasiswa::where('name', 'like', '%' . $search . '%')
            ->orderBy($sortBy, $orderBy)
            ->paginate($perPage);

        return $this->respond(200, $mahasiswa, 'Success');
    }

    public function create(Request $request) {
        $mahasiswa = Mahasiswa::create($request->all());
        return $this->respond(200, $mahasiswa, 'Success');
    }

    public function edit(Request $request, $id) {
        $mahasiswa = Mahasiswa::find($id);
        return $this->respond(200, $mahasiswa, 'Success');
    }

    public function update(Request $request, $id) {
        $mahasiswa = Mahasiswa::find($id);
        $res = $mahasiswa->update($request->all());
        return $this->respond(200, $res, 'Success');
    }

    public function delete($id) {
        $mahasiswa = Mahasiswa::destroy($id);
        return $this->respond(200, $mahasiswa, 'Success');
    }

    protected function respond($status, $data, $msg) {
        $datas = [
            'msg'  => $msg,
            'data' => $data
        ];

        return response()->json($datas, $status);
    }

    //
}
