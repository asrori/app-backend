<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('mahasiswa', 'MahasiswaController@index');
$router->get('mahasiswa/{id}', 'MahasiswaController@edit');
$router->post('mahasiswa', 'MahasiswaController@create');
$router->put('mahasiswa/{id}', 'MahasiswaController@update');
$router->delete('mahasiswa/{id}', 'MahasiswaController@delete');
